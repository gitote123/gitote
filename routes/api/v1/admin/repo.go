package admin

import (
	"gitote/gitote/pkg/context"
	"gitote/gitote/routes/api/v1/repo"
	"gitote/gitote/routes/api/v1/user"

	api "gitlab.com/gitote/go-gitote-client"
)

func CreateRepo(c *context.APIContext, form api.CreateRepoOption) {
	owner := user.GetUserByParams(c)
	if c.Written() {
		return
	}

	repo.CreateUserRepo(c, owner, form)
}
