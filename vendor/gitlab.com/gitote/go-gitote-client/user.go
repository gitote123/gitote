package gitote

import (
	"encoding/json"
	"fmt"
	"time"
)

// User represents a API user.
type User struct {
	ID               int64     `json:"id"`
	UserName         string    `json:"login"`
	FullName         string    `json:"full_name"`
	Website          string    `json:"website"`
	Email            string    `json:"email"`
	Company          string    `json:"company"`
	Location         string    `json:"location"`
	Description      string    `json:"bio"`
	IsAdmin          bool      `json:"site_admin"`
	NumRepos         int       `json:"repos"`
	Created          time.Time `json:"created_at"`
	Updated          time.Time `json:"updated_at"`
	NumFollowing     int       `json:"following"`
	NumFollowers     int       `json:"followers"`
	AvatarUrl        string    `json:"avatar_url"`
	FollowersURL     string    `json:"followers_url"`
	FollowingURL     string    `json:"following_url"`
	OrganizationsURL string    `json:"organizations_url"`
	ReposURL         string    `json:"repos_url"`
}

// MarshalJSON implements the json.Marshaler interface for User
func (u User) MarshalJSON() ([]byte, error) {
	// Re-declaring User to avoid recursion
	type shadow User
	return json.Marshal(struct {
		shadow
		// LEGACY [Gitote 1.0]: remove field(s) for backward compatibility
		CompatUserName string `json:"username"`
	}{shadow(u), u.UserName})
}

func (c *Client) GetUserInfo(user string) (*User, error) {
	u := new(User)
	err := c.getParsedResponse("GET", fmt.Sprintf("/users/%s", user), nil, nil, u)
	return u, err
}
