{{template "base/head" .}}
<div class="admin new authentication">
	<div class="ui container">
		<div class="ui grid">
			{{template "admin/navbar" .}}
			<div class="twelve wide column content">
				{{template "base/alert" .}}
				<h4 class="ui top attached header">
					Add New Source ➕
				</h4>
				<div class="ui attached segment">
					<form class="ui form" action="{{.Link}}" method="post">
						{{.CSRFTokenHTML}}
						<!-- Types and name -->
						<div class="inline required field {{if .Err_Type}}error{{end}}">
							<label>Authentication Type</label>
							<div class="ui selection type dropdown">
								<input type="hidden" id="auth_type" name="type" value="{{.type}}">
								<div class="text">{{.CurrentTypeName}}</div>
								<i class="dropdown icon"></i>
								<div class="menu">
									{{range .AuthSources}}
										<div class="item" data-value="{{.Type}}">{{.Name}}</div>
									{{end}}
								</div>
							</div>
						</div>
						<div class="required inline field {{if .Err_Name}}error{{end}}">
							<label for="name">Authentication Name</label>
							<input id="name" name="name" value="{{.name}}" autofocus required>
						</div>

						<!-- LDAP and DLDAP -->
						<div class="ldap dldap field {{if not (or (eq .type 2) (eq .type 5))}}hide{{end}}">
							<div class="inline required field {{if .Err_SecurityProtocol}}error{{end}}">
								<label>Security Protocol</label>
								<div class="ui selection security-protocol dropdown">
									<input type="hidden" id="security_protocol" name="security_protocol" value="{{.security_protocol}}">
									<div class="text">{{.CurrentSecurityProtocol}}</div>
									<i class="dropdown icon"></i>
									<div class="menu">
										{{range .SecurityProtocols}}
											<div class="item" data-value="{{.Type}}">{{.Name}}</div>
										{{end}}
									</div>
								</div>
							</div>
							<div class="required field">
								<label for="host">Host</label>
								<input id="host" name="host" value="{{.host}}" placeholder="e.g. mydomain.com">
							</div>
							<div class="required field">
								<label for="port">Port</label>
								<input id="port" name="port" value="{{.port}}"  placeholder="e.g. 636">
							</div>
							<div class="ldap field {{if not (eq .type 2)}}hide{{end}}">
								<label for="bind_dn">Bind DN</label>
								<input id="bind_dn" name="bind_dn" value="{{.bind_dn}}" placeholder="e.g. cn=Search,dc=mydomain,dc=com">
								<p class="help text blue">You can use '%s' as placeholder for username, e.g. DOM\%s</p>
							</div>
							<div class="ldap field {{if not (eq .type 2)}}hide{{end}}">
								<label for="bind_password">Bind Password</label>
								<input id="bind_password" name="bind_password" type="password" value="{{.bind_password}}">
								<p class="help text red">Warning: This password is stored in plain text. Do not use a high privileged account.</p>
							</div>
							<div class="ldap required field {{if not (eq .type 2)}}hide{{end}}">
								<label for="user_base">User Search Base</label>
								<input id="user_base" name="user_base" value="{{.user_base}}" placeholder="e.g. ou=Users,dc=mydomain,dc=com">
							</div>
							<div class="dldap required field {{if not (eq .type 5)}}hide{{end}}">
								<label for="user_dn">User DN</label>
								<input id="user_dn" name="user_dn" value="{{.user_dn}}" placeholder="e.g. uid=%s,ou=Users,dc=mydomain,dc=com">
							</div>
							<div class="required field">
								<label for="filter">User Filter</label>
								<input id="filter" name="filter" value="{{.filter}}" placeholder="e.g. (&(objectClass=posixAccount)(uid=%s))">
							</div>
							<div class="field">
								<label for="admin_filter">Admin Filter</label>
								<input id="admin_filter" name="admin_filter" value="{{.admin_filter}}">
							</div>
							<div class="field">
								<label for="attribute_username">Username Attribute</label>
								<input id="attribute_username" name="attribute_username" value="{{.attribute_username}}" placeholder="Leave empty to use sign-in form field value for user name.">
							</div>
							<div class="field">
								<label for="attribute_name">First Name Attribute</label>
								<input id="attribute_name" name="attribute_name" value="{{.attribute_name}}">
							</div>
							<div class="field">
								<label for="attribute_surname">Surname Attribute</label>
								<input id="attribute_surname" name="attribute_surname" value="{{.attribute_surname}}">
							</div>
							<div class="required field">
								<label for="attribute_mail">Email Attribute</label>
								<input id="attribute_mail" name="attribute_mail" value="{{.attribute_mail}}" placeholder="e.g. mail">
							</div>
							
							<div class="inline field">
								<div class="ui checkbox">
									<label><strong>Verify group membership</strong></label>
									<input class="enable-system" type="checkbox" name="group_enabled" data-target="#group_box" {{if .group_enabled}}checked{{end}}>
								</div>
							</div>
							<div class="ui segment field {{if not .group_enabled}}disabled{{end}}" id="group_box">
								<div class="field">
									<label for="group_dn">Group Search Base DN</label>
									<input id="group_dn" name="group_dn" value="{{.group_dn}}" placeholder="e.g. ou=group,dc=mydomain,dc=com">
								</div>
								<div class="field">
									<label for="group_filter">Group Filter</label>
									<input id="group_filter" name="group_filter" value="{{.group_filter}}" placeholder="e.g. (|(cn=gitote_users)(cn=admins))">
								</div>
								<div class="field">
									<label for="group_member_uid">Group Attribute Containing List of Users</label>
									<input id="group_member_uid" name="group_member_uid" value="{{.group_member_uid}}" placeholder="e.g. memberUid">
								</div>
								<div class="field">
									<label for="user_uid">User Attribute Listed in Group</label>
									<input id="user_uid" name="user_uid" value="{{.user_uid}}" placeholder="e.g. uid">
								</div>
							</div>
						</div>

						<!-- SMTP -->
						<div class="smtp field {{if not (eq .type 3)}}hide{{end}}">
							<div class="inline required field">
								<label>SMTP Authentication Type</label>
								<div class="ui selection type dropdown">
									<input type="hidden" id="smtp_auth" name="smtp_auth" value="{{.smtp_auth}}">
									<div class="text">{{.smtp_auth}}</div>
									<i class="dropdown icon"></i>
									<div class="menu">
										{{range .SMTPAuths}}
											<div class="item" data-value="{{.}}">{{.}}</div>
										{{end}}
									</div>
								</div>
							</div>
							<div class="required field">
								<label for="smtp_host">SMTP Host</label>
								<input id="smtp_host" name="smtp_host" value="{{.smtp_host}}">
							</div>
							<div class="required field">
								<label for="smtp_port">SMTP Port</label>
								<input id="smtp_port" name="smtp_port" value="{{.smtp_port}}">
							</div>
							<div class="field">
								<label for="allowed_domains">Allowed Domains</label>
								<input id="allowed_domains" name="allowed_domains" value="{{.allowed_domains}}">
								<p class="help">Leave it empty to not restrict any domains. Multiple domains should be separated by comma ','.</p>
							</div>
						</div>

						<!-- PAM -->
						<div class="pam required field {{if not (eq .type 4)}}hide{{end}}">
							<label for="pam_service_name">PAM Service Name</label>
							<input id="pam_service_name" name="pam_service_name" value="{{.pam_service_name}}" />
						</div>

						<div class="ldap field">
							<div class="ui checkbox">
								<label><strong>Fetch attributes in Bind DN context</strong></label>
								<input name="attributes_in_bind" type="checkbox" {{if .attributes_in_bind}}checked{{end}}>
							</div>
						</div>
						<div class="smtp inline field {{if not (eq .type 3)}}hide{{end}}">
							<div class="ui checkbox">
								<label><strong>Enable TLS Encryption</strong></label>
								<input name="tls" type="checkbox" {{if .tls}}checked{{end}}>
							</div>
						</div>
						<div class="has-tls inline field {{if not .HasTLS}}hide{{end}}">
							<div class="ui checkbox">
								<label><strong>Skip TLS Verify</strong></label>
								<input name="skip_verify" type="checkbox" {{if .skip_verify}}checked{{end}}>
							</div>
						</div>

						<div class="inline field">
							<div class="ui checkbox">
								<label><strong>This authentication is activated</strong></label>
								<input name="is_active" type="checkbox" {{if .is_active}}checked{{end}}>
							</div>
						</div>

						<div class="inline field">
							<div class="ui checkbox">
								<label><strong>This authentication is default login source</strong></label>
								<input name="is_default" type="checkbox" {{if .is_default}}checked{{end}}>
							</div>
						</div>

						<div class="field">
							<button class="ui green button">New authentication has been added successfully.</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
{{template "base/footer" .}}
