FROM golang:alpine AS builder

MAINTAINER "Yoginth <yoginth@protonmail.com>"

# Install build deps
RUN apk --no-cache --no-progress add --virtual build-deps build-base git
WORKDIR /go/src/gitote/gitote
COPY . .
RUN make build

FROM alpine:latest

# Install system utils & Gitote runtime dependencies
ADD https://github.com/tianon/gosu/releases/download/1.10/gosu-amd64 /usr/sbin/gosu
RUN chmod +x /usr/sbin/gosu \
  && echo http://dl-2.alpinelinux.org/alpine/edge/community/ >> /etc/apk/repositories \
  && apk --no-cache --no-progress add \
    bash \
    ca-certificates \
    curl \
    git \
    openssh \
    s6 \
    shadow \
    socat \
    tzdata

ENV GITOTE_CUSTOM /data/gitote

# Configure LibC Name Service
COPY docker/nsswitch.conf /etc/nsswitch.conf

WORKDIR /app/gitote
COPY docker ./docker
COPY templates ./templates
COPY public ./public
COPY --from=builder /go/src/gitote/gitote .

RUN ./docker/finalize.sh

# Configure Docker Container
VOLUME ["/data"]
EXPOSE 22 3000
ENTRYPOINT ["/app/gitote/docker/start.sh"]
CMD ["/bin/s6-svscan", "/app/gitote/docker/s6/"]
