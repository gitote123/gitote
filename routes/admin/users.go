package admin

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"gitote/gitote/pkg/form"
	"gitote/gitote/pkg/mailer"
	"gitote/gitote/pkg/setting"
	"gitote/gitote/routes"
	"strings"

	"github.com/Unknwon/com"
	log "gopkg.in/clog.v1"
)

const (
	// USERS list page template
	USERS = "admin/user/list"

	// USER_NEW page template
	USER_NEW = "admin/user/new"

	// USER_EDIT page template
	USER_EDIT = "admin/user/edit"
)

// Users shows users page
func Users(c *context.Context) {
	c.Data["Title"] = "Users"
	c.Data["PageIsAdmin"] = true
	c.Data["PageIsAdminUsers"] = true

	routes.RenderUserSearch(c, &routes.UserSearchOptions{
		Type:     models.USER_TYPE_INDIVIDUAL,
		Counter:  models.CountUsers,
		Ranger:   models.Users,
		PageSize: setting.UI.Admin.UserPagingNum,
		OrderBy:  "id ASC",
		TplName:  USERS,
	})
}

// NewUser shows new user page
func NewUser(c *context.Context) {
	c.Data["Title"] = "Create New Account"
	c.Data["PageIsAdmin"] = true
	c.Data["PageIsAdminUsers"] = true

	c.Data["login_type"] = "0-0"

	sources, err := models.LoginSources()
	if err != nil {
		c.Handle(500, "LoginSources", err)
		return
	}
	c.Data["Sources"] = sources

	c.Data["CanSendEmail"] = setting.MailService != nil
	c.HTML(200, USER_NEW)
}

// NewUserPost creates a new user
func NewUserPost(c *context.Context, f form.AdminCrateUser) {
	c.Data["Title"] = "Create New Account"
	c.Data["PageIsAdmin"] = true
	c.Data["PageIsAdminUsers"] = true

	sources, err := models.LoginSources()
	if err != nil {
		c.Handle(500, "LoginSources", err)
		return
	}
	c.Data["Sources"] = sources

	c.Data["CanSendEmail"] = setting.MailService != nil

	if c.HasError() {
		c.HTML(200, USER_NEW)
		return
	}

	u := &models.User{
		Name:       f.UserName,
		Email:      f.Email,
		Passwd:     f.Password,
		ThemeColor: "#161616",
		IsActive:   true,
		ShowAds:    true,
		LoginType:  models.LOGIN_PLAIN,
	}

	if len(f.LoginType) > 0 {
		fields := strings.Split(f.LoginType, "-")
		if len(fields) == 2 {
			u.LoginType = models.LoginType(com.StrTo(fields[0]).MustInt())
			u.LoginSource = com.StrTo(fields[1]).MustInt64()
			u.LoginName = f.LoginName
		}
	}

	if err := models.CreateUser(u); err != nil {
		switch {
		case models.IsErrUserAlreadyExist(err):
			c.Data["Err_UserName"] = true
			c.RenderWithErr(c.Tr("form.username_been_taken"), USER_NEW, &f)
		case models.IsErrEmailAlreadyUsed(err):
			c.Data["Err_Email"] = true
			c.RenderWithErr(c.Tr("form.email_been_used"), USER_NEW, &f)
		case models.IsErrNameReserved(err):
			c.Data["Err_UserName"] = true
			c.RenderWithErr(c.Tr("user.form.name_reserved", err.(models.ErrNameReserved).Name), USER_NEW, &f)
		case models.IsErrNamePatternNotAllowed(err):
			c.Data["Err_UserName"] = true
			c.RenderWithErr(c.Tr("user.form.name_pattern_not_allowed", err.(models.ErrNamePatternNotAllowed).Pattern), USER_NEW, &f)
		default:
			c.Handle(500, "CreateUser", err)
		}
		return
	}
	log.Trace("Account created by admin (%s): %s", c.User.Name, u.Name)

	// Send email notification.
	if f.SendNotify && setting.MailService != nil {
		mailer.SendRegisterNotifyMail(c.Context, models.NewMailerUser(u))
	}

	c.Flash.Success(c.Tr("admin.users.new_success", u.Name))
	c.Redirect(setting.AppSubURL + "/admin/users/" + com.ToStr(u.ID))
}

func prepareUserInfo(c *context.Context) *models.User {
	u, err := models.GetUserByID(c.ParamsInt64(":userid"))
	if err != nil {
		c.Handle(500, "GetUserByID", err)
		return nil
	}
	c.Data["User"] = u

	if u.LoginSource > 0 {
		c.Data["LoginSource"], err = models.GetLoginSourceByID(u.LoginSource)
		if err != nil {
			c.Handle(500, "GetLoginSourceByID", err)
			return nil
		}
	} else {
		c.Data["LoginSource"] = &models.LoginSource{}
	}

	sources, err := models.LoginSources()
	if err != nil {
		c.Handle(500, "LoginSources", err)
		return nil
	}
	c.Data["Sources"] = sources

	return u
}

// EditUser shows edit user page
func EditUser(c *context.Context) {
	c.Data["Title"] = "Edit Account"
	c.Data["PageIsAdmin"] = true
	c.Data["PageIsAdminUsers"] = true
	c.Data["EnableLocalPathMigration"] = setting.Repository.EnableLocalPathMigration

	prepareUserInfo(c)
	if c.Written() {
		return
	}

	c.HTML(200, USER_EDIT)
}

// EditUserPost edits an user
func EditUserPost(c *context.Context, f form.AdminEditUser) {
	c.Data["Title"] = "Edit Account"
	c.Data["PageIsAdmin"] = true
	c.Data["PageIsAdminUsers"] = true
	c.Data["EnableLocalPathMigration"] = setting.Repository.EnableLocalPathMigration

	u := prepareUserInfo(c)
	if c.Written() {
		return
	}

	if c.HasError() {
		c.HTML(200, USER_EDIT)
		return
	}

	fields := strings.Split(f.LoginType, "-")
	if len(fields) == 2 {
		loginType := models.LoginType(com.StrTo(fields[0]).MustInt())
		loginSource := com.StrTo(fields[1]).MustInt64()

		if u.LoginSource != loginSource {
			u.LoginSource = loginSource
			u.LoginType = loginType
		}
	}

	if len(f.Password) > 0 {
		u.Passwd = f.Password
		var err error
		if u.Salt, err = models.GetUserSalt(); err != nil {
			c.Handle(500, "UpdateUser", err)
			return
		}
		u.EncodePasswd()
	}

	u.LoginName = f.LoginName
	u.FullName = f.FullName
	u.Company = f.Company
	u.Description = f.Description
	u.Email = f.Email
	u.Website = f.Website
	u.Location = f.Location
	u.StaffNotes = f.StaffNotes
	u.MaxRepoCreation = f.MaxRepoCreation
	u.IsActive = f.Active
	u.IsAdmin = f.Admin
	u.AllowGitHook = f.AllowGitHook
	u.AllowImportLocal = f.AllowImportLocal
	u.Suspended = f.Suspended
	u.IsVerified = f.IsVerified
	u.IsMaker = f.IsMaker
	u.IsBugHunter = f.IsBugHunter
	u.GitoteDeveloper = f.GitoteDeveloper
	u.IsBeta = f.IsBeta
	u.IsStaff = f.IsStaff
	u.IsIntern = f.IsIntern
	u.Recognized = f.Recognized
	u.Certified = f.Certified
	u.Twitter = f.Twitter
	u.Linkedin = f.Linkedin
	u.Github = f.Github
	u.Devto = f.Devto
	u.Stackoverflow = f.Stackoverflow
	u.Reddit = f.Reddit
	u.Telegram = f.Telegram
	u.Codepen = f.Codepen
	u.Gitlab = f.Gitlab

	if err := models.UpdateUser(u); err != nil {
		if models.IsErrEmailAlreadyUsed(err) {
			c.Data["Err_Email"] = true
			c.RenderWithErr(c.Tr("form.email_been_used"), USER_EDIT, &f)
		} else {
			c.Handle(500, "UpdateUser", err)
		}
		return
	}
	log.Trace("Account profile updated by admin (%s): %s", c.User.Name, u.Name)

	c.Flash.Success("Account profile has been updated successfully.")
	c.Redirect(setting.AppSubURL + "/admin/users/" + c.Params(":userid"))
}

// DeleteUser deletes an user
func DeleteUser(c *context.Context) {
	u, err := models.GetUserByID(c.ParamsInt64(":userid"))
	if err != nil {
		c.Handle(500, "GetUserByID", err)
		return
	}

	if err = models.DeleteUser(u); err != nil {
		switch {
		case models.IsErrUserOwnRepos(err):
			c.Flash.Error("This account still has ownership over at least one repository, you have to delete or transfer them first.")
			c.JSON(200, map[string]interface{}{
				"redirect": setting.AppSubURL + "/admin/users/" + c.Params(":userid"),
			})
		case models.IsErrUserHasOrgs(err):
			c.Flash.Error("This account still has membership in at least one organization, you have to leave or delete the organizations first.")
			c.JSON(200, map[string]interface{}{
				"redirect": setting.AppSubURL + "/admin/users/" + c.Params(":userid"),
			})
		default:
			c.Handle(500, "DeleteUser", err)
		}
		return
	}
	log.Trace("Account deleted by admin (%s): %s", c.User.Name, u.Name)

	c.Flash.Success("Account has been deleted successfully!")
	c.JSON(200, map[string]interface{}{
		"redirect": setting.AppSubURL + "/admin/users",
	})
}
