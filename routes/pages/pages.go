package pages

import (
	"gitote/gitote/pkg/context"
)

const (
	// ABOUT page template
	ABOUT = "pages/about"

	// FAQ page template
	FAQ = "pages/faq"

	// PRIVACY page template
	PRIVACY = "pages/privacy"

	// TOS page template
	TOS = "pages/tos"

	// BRAND page template
	BRAND = "pages/brand"

	// CONTACT page template
	CONTACT = "pages/contact"

	// CONTRIBUTE page template
	CONTRIBUTE = "pages/contribute"

	// SECURITY page template
	SECURITY = "pages/security"

	// VERIFIED page template
	VERIFIED = "pages/verified"

	// MAKERS page template
	MAKERS = "pages/makers"

	// HELP page template
	HELP = "pages/help"

	// FEATURES page template
	FEATURES = "pages/features"

	// FEATUREREQUEST page template
	FEATUREREQUEST = "pages/request"

	// SPONSORSHIP page template
	SPONSORSHIP = "pages/sponsorship"

	// SITEMAP page template
	SITEMAP = "pages/sitemap"
)

// About shows about page
func About(c *context.Context) {
	c.Data["Title"] = "About"

	c.HTML(200, ABOUT)
}

// Faq shows faq page
func Faq(c *context.Context) {
	c.Data["Title"] = "FAQ"

	c.HTML(200, FAQ)
}

// Privacy shows privacy page
func Privacy(c *context.Context) {
	c.Data["Title"] = "Privacy Policy"

	c.HTML(200, PRIVACY)
}

// Tos shows tos page
func Tos(c *context.Context) {
	c.Data["Title"] = "Terms of Service"

	c.HTML(200, TOS)
}

// Brand shows brand page
func Brand(c *context.Context) {
	c.Data["Title"] = "Gitote Brand"

	c.HTML(200, BRAND)
}

// Contribute shows contribute page
func Contribute(c *context.Context) {
	c.Data["PageIsContribute"] = true
	c.Data["Title"] = "Contribute"

	c.HTML(200, CONTRIBUTE)
}

// Security shows security page
func Security(c *context.Context) {
	c.Data["Title"] = "Security"

	c.HTML(200, SECURITY)
}

// Verified shows verified page
func Verified(c *context.Context) {
	c.Data["Title"] = "Verified"

	c.HTML(200, VERIFIED)
}

// Makers shows maker page
func Makers(c *context.Context) {
	c.Data["Title"] = "Makers"

	c.HTML(200, MAKERS)
}

// Help shows help page
func Help(c *context.Context) {
	c.Data["Title"] = "Help"
	c.Data["PageIsHelp"] = true

	c.HTML(200, HELP)
}

// Contact shows contact page
func Contact(c *context.Context) {
	c.Data["Title"] = "Contact"

	c.HTML(200, CONTACT)
}

// Features shows features page
func Features(c *context.Context) {
	c.Data["Title"] = "Features"
	c.Data["PageIsFeatures"] = true

	c.HTML(200, FEATURES)
}

// FeatureRequest shows feature request page
func FeatureRequest(c *context.Context) {
	c.Data["Title"] = "Feature Request"

	c.HTML(200, FEATUREREQUEST)
}

// Sponsorship shows sponsorship page
func Sponsorship(c *context.Context) {
	c.Data["Title"] = "Sponsorship"

	c.HTML(200, SPONSORSHIP)
}
