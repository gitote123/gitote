package user

import (
	"gitote/gitote/pkg/context"
	"strings"
)

const (
	INTERNCERTIFICATE = "misc/internscert"
)

func InternCertificate(c *context.Context) {
	ctxUser := GetUserByName(c, strings.TrimSuffix(c.Params(":username"), ""))
	c.Data["Owner"] = ctxUser

	c.HTML(200, INTERNCERTIFICATE)
}
