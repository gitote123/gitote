# Git + Rem(ote) = Gitote ❤️
#### Software version control made simple!

Welcome to the [gitote](https://gitote.in) codebase. We are so excited to have you. With your help, we can build out Gitote to be more stable and better serve our platform.

#### Shields 🛡

Linux | Windows | Go Report | Codacy | Donate | Discord
----- | ------- | --------- | ------ | ------ | -------
[![GitLab Build status](https://img.shields.io/gitlab/pipeline/gitote/gitote.svg?logo=gitlab)](https://gitote.in/gitote/gitote) | [![AppVeyor Build status](https://img.shields.io/appveyor/ci/yoginth/gitote.svg?logo=appveyor)](https://ci.appveyor.com/project/yoginth/gitote) | [![Go Report](https://goreportcard.com/badge/gitote.in/gitote/gitote)](https://goreportcard.com/report/gitote.in/gitote/gitote) | [![Codacy Badge](https://api.codacy.com/project/badge/Grade/3caea410c31745959a9538a44b242516)](https://www.codacy.com/app/Gitote/gitote?utm_source=gitote.in&amp;utm_medium=referral&amp;utm_content=gitote/gitote&amp;utm_campaign=Badge_Grade) | [![Liberapay](https://img.shields.io/liberapay/receives/gitote.svg?logo=liberapay&maxAge=3600)](https://liberapay.com/gitote/donate) | [![Discord](https://img.shields.io/badge/chat-on%20discord-7289da.svg?logo=discord&maxAge=3600)](https://discord.gg/wEUXMJp)

#### Docker 🐬

Downloads | Size | Layers | Quay | Azure
--------- | ---- | ------ | ---- | -----
[![Downloads](https://img.shields.io/docker/pulls/gitote/gitote.svg?logo=docker&maxAge=3600)](https://store.docker.com/community/images/gitote/gitote) | [![Size](https://img.shields.io/microbadger/image-size/gitote/gitote.svg?logo=docker&maxAge=3600)](https://store.docker.com/community/images/gitote/gitote) | [![Layers](https://img.shields.io/microbadger/layers/gitote/gitote.svg?logo=docker&maxAge=3600)](https://store.docker.com/community/images/gitote/gitote) | [![Quay](https://quay.io/repository/gitote/gitote/status)](https://quay.io/repository/gitote/gitote) | [![Build status](https://dev.azure.com/gitote/gitote/_apis/build/status/Gitote%20CI)](https://dev.azure.com/gitote/gitote/_build/latest?definitionId=1)

#### Versions 📦

GoLang | PostgreSQL | Git
------ | ---------- | ---
[![GoLang](https://img.shields.io/badge/GoLang-v1.7-green.svg?logo=go&maxAge=3600)](https://golang.org) | [![PostgreSQL](https://img.shields.io/badge/PostgreSQL-v9.5-green.svg?logo=postgresql&maxAge=3600)](https://www.postgresql.org) | [![Git](https://img.shields.io/badge/Git-v1.7.1-green.svg?logo=git&maxAge=3600)](https://git-scm.com/)

## What is gitote ❓

[Gitote](https://gitote.in) is an open source end-to-end software development platform with built-in version control, issue tracking, code review, and more.

## Contributing 🚧

We expect contributors to abide by our underlying [code of conduct](CONDUCT.md). All conversations and discussions on Gitote (issues, pull requests) and across Gitote must be respectful and harassment-free.

### Where to contribute

When in doubt, ask a [core team member](#core-team)! You can mention us in any issues . Any issue with [`Good first Issue`](https://gitote.in/gitote/gitote/issues?labels=9) tag is typically a good place to start.

✨ **Refactoring code**, e.g. improving the code without modifying the behavior is an area that can probably be done based on intuition and may not require much communication to be merged.

🐞 **Fixing bugs** may also not require a lot of communication, but the more the better. Please surround bug fixes with ample tests. Bugs are magnets for other bugs. Write tests near bugs!

🏗 **Building features** is the area which will require the most communication and/or negotiation. Every feature is subjective and open for debate. As always, when in doubt, ask!

### How to contribute

1.  Fork the project & clone locally. Follow the initial setup [here](docs/INSTALLATION.md).
2.  Create a branch, naming it either a feature or bug: `git checkout -b feature/that-new-feature` or `bug/fixing-that-bug`
3.  Code and commit your changes. Bonus points if you write a [good commit message](https://chris.beams.io/posts/git-commit/): `git commit -m 'Add some feature'`
4.  Push to the branch: `git push origin feature/that-new-feature`
5.  [Create a pull request](#create-a-pull-request) for your branch 🎉

## Contribution guideline 📜

### Create an issue

Nobody's perfect. Something doesn't work? Or could be done better? Let us know by creating an issue.

PS: a clear and detailed issue gets lots of love, all you have to do is follow the issue template!

#### Clean code with tests

Some existing code may be poorly written or untested, so we must have more scrutiny going forward. We test with **go test**, let us know if you have any questions about this!

#### Create a pull request

* Try to keep the pull requests small. A pull request should try its very best to address only a single concern.
* Make sure all tests pass and add additional tests for the code you submit.
* Document your reasoning behind the changes. Explain why you wrote the code in the way you did. The code should explain what it does.
* If there's an existing issue related to the pull request, reference to it by adding something like `References/Closes/Resolves #305`, where 305 is the issue number.
* If you follow the pull request template, you can't go wrong.

## Codebase 💻

### The stack

#### Open Source

- [GoLang](https://golang.org/)
- [PostgreSQL](https://www.postgresql.org/)
- [XORM](http://xorm.io/)
- [Semantic UI](https://semantic-ui.com/)
- [GitHub Octicons](https://octicons.github.com/)
- [Font Awesome](https://fontawesome.io/)
- [jQuery](https://jquery.com/)

#### Infra/Services

- [Gitote](https://gitote.in) - VCS
- [DigitalOcean](https://m.do.co/c/1ddec8d50826) - Hosting
- [Ubuntu 18.10 x64](https://www.ubuntu.com/) - Operating System
- [NGINX](https://www.nginx.com/) - Server
- [Let's Encrypt](https://letsencrypt.org/) - SSL
- [Google Analytics](https://analytics.google.com/analytics/web/) - Web Analytics
- [GitLab CI](https://about.gitlab.com/product/continuous-integration/) - Continuous Integration
- [BrowserStack](https://www.browserstack.com) - Cross-platform Browser testing
- [jsDelivr](https://www.jsdelivr.com/) - Static Assets CDN
- [Gravatar](https://gravatar.com/) - Globally Recognized Avatar
- [Heroku Toolbelt](https://blog.heroku.com/the_heroku_toolbelt) - For development(one-click launch)

#### Ops

- OteBot _under development_ **Similar to GitHub's Hubot**
- [Slack](https://gitote.slack.com/)

## Features 🔮

- Activity timeline
- **SSH** and **HTTP/HTTPS** protocols
- Account/Organization/Repository management
- Add/Remove repository collaborators
- Repository/Organization **webhooks** (including Slack and Discord)
- Repository **Git hooks/deploy keys**
- Repository **issues**, **pull requests**, **wiki** and **protected branches**
- **Migrate** and **mirror** repository and its wiki
- **Web editor** for repository files and wiki
- **Jupyter Notebook**
- **Two-factor authentication**
- **Gravatar** and **Federated avatar** with custom source
- **Mail** service

## Core team 👬

- [@yogi](https://gitote.in/yogi)
- [@dimensi0n](https://gitote.in/dimensi0n)
- [@dantehemerson](https://gitote.in/dantehemerson)

## Sponsors 🙏

#### [![DigitalOcean](https://img.shields.io/badge/DigitalOcean-%F0%9F%99%8F-555.svg?logo=digitalocean&style=for-the-badge)](https://m.do.co/c/1ddec8d50826)
#### [![BrowserStack](https://img.shields.io/badge/BrowserStack-%F0%9F%99%8F-555.svg?logo=google%20chrome&style=for-the-badge)](https://browserstack.com/?utm_source=Gitote)
#### [![Sentry](https://img.shields.io/badge/Sentry-%F0%9F%99%8F-555.svg?logo=sentry&style=for-the-badge)](https://sentry.io/?utm_source=Gitote)

## License 💼

This program is free software: you can redistribute it and/or modify it under the terms of the **MIT License**. Please see the [LICENSE](https://gitote.in/gitote/gitote/src/master/LICENSE) file in our repository for the full text.

###### Happy Coding ❤️
