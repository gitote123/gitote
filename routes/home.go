package routes

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"gitote/gitote/pkg/setting"
	"gitote/gitote/routes/user"

	"gitlab.com/yoginth/paginater"
)

const (
	// HOME page template
	HOME = "home"

	// EXPLORE_HOME page template
	EXPLORE_HOME = "explore/home"

	// EXPLORE_REPOS page template
	EXPLORE_REPOS = "explore/repos"

	// EXPLORE_USERS page template
	EXPLORE_USERS = "explore/users"

	// EXPLORE_TRENDING page template
	EXPLORE_TRENDING = "explore/trending"

	// EXPLORE_ORGANIZATIONS page template
	EXPLORE_ORGANIZATIONS = "explore/organizations"
)

// Home shows the home page
func Home(c *context.Context) {
	if c.IsLogged {
		if !c.User.IsActive && setting.Service.RegisterEmailConfirm {
			c.Data["Title"] = c.Tr("auth.active_your_account")
			c.Success(user.ACTIVATE)
		} else {
			user.Dashboard(c)
		}
		return
	}

	// Check auto-login.
	uname := c.GetCookie(setting.CookieUserName)
	if len(uname) != 0 {
		c.Redirect(setting.AppSubURL + "/login")
		return
	}

	c.Data["PageIsHome"] = true
	c.Success(HOME)
}

// ExploreHome shows explore page
func ExploreHome(c *context.Context) {
	c.Data["Title"] = c.Tr("explore")
	c.Data["PageIsExplore"] = true
	c.Data["PageIsExploreRepositories"] = true

	page := c.QueryInt("page")
	if page <= 0 {
		page = 1
	}

	keyword := c.Query("q")
	repos, count, err := models.SearchRepositoryByName(&models.SearchRepoOptions{
		Keyword:  keyword,
		UserID:   c.UserID(),
		OrderBy:  "updated_unix DESC",
		Page:     page,
		PageSize: setting.UI.ExplorePagingNum,
	})
	if err != nil {
		c.ServerError("SearchRepositoryByName", err)
		return
	}
	c.Data["Keyword"] = keyword
	c.Data["Total"] = count
	c.Data["Page"] = paginater.New(int(count), setting.UI.ExplorePagingNum, page, 5)

	if err = models.RepositoryList(repos).LoadAttributes(); err != nil {
		c.ServerError("RepositoryList.LoadAttributes", err)
		return
	}
	c.Data["Repos"] = repos

	c.Success(EXPLORE_HOME)
}

// ExploreRepos shows explore repositories page
func ExploreRepos(c *context.Context) {
	c.Data["Title"] = c.Tr("explore")
	c.Data["PageIsExplore"] = true
	c.Data["PageIsExploreRepositories"] = true

	page := c.QueryInt("page")
	if page <= 0 {
		page = 1
	}

	keyword := c.Query("q")
	// Search a repository
	repos, count, err := models.SearchRepositoryByName(&models.SearchRepoOptions{
		Keyword:  keyword,
		UserID:   c.UserID(),
		OrderBy:  "updated_unix DESC",
		Page:     page,
		PageSize: setting.UI.ExplorePagingNum,
	})
	if err != nil {
		c.ServerError("SearchRepositoryByName", err)
		return
	}
	c.Data["Keyword"] = keyword
	c.Data["Total"] = count
	c.Data["Page"] = paginater.New(int(count), setting.UI.ExplorePagingNum, page, 5)

	if err = models.RepositoryList(repos).LoadAttributes(); err != nil {
		c.ServerError("RepositoryList.LoadAttributes", err)
		return
	}
	c.Data["Repos"] = repos

	c.Success(EXPLORE_REPOS)
}

// ExploreTrending shows trending repositories page
func ExploreTrending(c *context.Context) {
	c.Data["Title"] = c.Tr("explore.trending")
	c.Data["PageIsTrending"] = true

	repos, count, err := models.SearchRepositoryByName(&models.SearchRepoOptions{
		UserID:   c.UserID(),
		OrderBy:  "num_stars DESC",
		PageSize: 15,
	})
	if err != nil {
		c.ServerError("SearchRepositoryByName", err)
		return
	}
	c.Data["Total"] = count

	if err = models.RepositoryList(repos).LoadAttributes(); err != nil {
		c.ServerError("RepositoryList.LoadAttributes", err)
		return
	}
	c.Data["Repos"] = repos

	c.Success(EXPLORE_TRENDING)
}

// UserSearchOptions is the structure of the options choosed by the user
type UserSearchOptions struct {
	Type     models.UserType
	Counter  func() int64
	Ranger   func(int, int) ([]*models.User, error)
	PageSize int
	OrderBy  string
	TplName  string
}

// RenderUserSearch renders user search
func RenderUserSearch(c *context.Context, opts *UserSearchOptions) {
	page := c.QueryInt("page")
	if page <= 1 {
		page = 1
	}

	var (
		users []*models.User
		count int64
		err   error
	)

	keyword := c.Query("q")
	if len(keyword) == 0 {
		users, err = opts.Ranger(page, opts.PageSize)
		if err != nil {
			c.ServerError("Ranger", err)
			return
		}
		count = opts.Counter()
	} else {
		users, count, err = models.SearchUserByName(&models.SearchUserOptions{
			Keyword:  keyword,
			Type:     opts.Type,
			OrderBy:  opts.OrderBy,
			Page:     page,
			PageSize: opts.PageSize,
		})
		if err != nil {
			c.ServerError("SearchUserByName", err)
			return
		}
	}
	c.Data["Keyword"] = keyword
	c.Data["Total"] = count
	c.Data["Page"] = paginater.New(int(count), opts.PageSize, page, 5)
	c.Data["Users"] = users

	c.Success(opts.TplName)
}

// ExploreUsers shows explore users page
func ExploreUsers(c *context.Context) {
	c.Data["Title"] = c.Tr("explore")
	c.Data["PageIsExplore"] = true
	c.Data["PageIsExploreUsers"] = true

	RenderUserSearch(c, &UserSearchOptions{
		Type:     models.USER_TYPE_INDIVIDUAL,
		Counter:  models.CountUsers,
		Ranger:   models.Users,
		PageSize: setting.UI.ExplorePagingNum,
		OrderBy:  "updated_unix DESC",
		TplName:  EXPLORE_USERS,
	})
}

//ExploreOrganizations shows explore organizations page
func ExploreOrganizations(c *context.Context) {
	c.Data["Title"] = c.Tr("explore")
	c.Data["PageIsExplore"] = true
	c.Data["PageIsExploreOrganizations"] = true

	RenderUserSearch(c, &UserSearchOptions{
		Type:     models.USER_TYPE_ORGANIZATION,
		Counter:  models.CountOrganizations,
		Ranger:   models.Organizations,
		PageSize: setting.UI.ExplorePagingNum,
		OrderBy:  "updated_unix DESC",
		TplName:  EXPLORE_ORGANIZATIONS,
	})
}

// NotFound renders 404 page if page not found
func NotFound(c *context.Context) {
	c.Data["Title"] = "Page Not Found"
	c.NotFound()
}
