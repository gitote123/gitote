{{template "base/head" .}}
<div class="admin edit authentication">
	<div class="ui container">
		<div class="ui grid">
			{{template "admin/navbar" .}}
			<div class="twelve wide column content">
				{{template "base/alert" .}}
				<h4 class="ui top attached header">
					Edit Authentication Setting 🖊
				</h4>
				<div class="ui attached segment">
					<form class="ui form" action="{{.Link}}" method="post">
						{{.CSRFTokenHTML}}
						<input type="hidden" name="id" value="{{.Source.ID}}">
						<div class="inline field">
							<label>Authentication Type</label>
							<input type="hidden" id="auth_type" name="type" value="{{.Source.Type}}">
							<span>{{.Source.TypeName}}</span>
						</div>
						<div class="required inline field {{if .Err_Name}}error{{end}}">
							<label for="name">Authentication Name</label>
							<input id="name" name="name" value="{{.Source.Name}}" autofocus required>
						</div>

						<!-- LDAP and DLDAP -->
						{{if or .Source.IsLDAP .Source.IsDLDAP}}
							{{ $cfg:=.Source.LDAP }}
							<div class="inline required field {{if .Err_SecurityProtocol}}error{{end}}">
								<label>Security Protocol</label>
								<div class="ui selection security-protocol dropdown">
									<input type="hidden" id="security_protocol" name="security_protocol" value="{{$cfg.SecurityProtocol}}">
									<div class="text">{{$cfg.SecurityProtocolName}}</div>
									<i class="dropdown icon"></i>
									<div class="menu">
										{{range .SecurityProtocols}}
											<div class="item" data-value="{{.Type}}">{{.Name}}</div>
										{{end}}
									</div>
								</div>
							</div>
							<div class="required field">
								<label for="host">Host</label>
								<input id="host" name="host" value="{{$cfg.Host}}" placeholder="e.g. mydomain.com" required>
							</div>
							<div class="required field">
								<label for="port">Port</label>
								<input id="port" name="port" value="{{$cfg.Port}}"  placeholder="e.g. 636" required>
							</div>
							{{if .Source.IsLDAP}}
								<div class="field">
									<label for="bind_dn">Bind DN</label>
									<input id="bind_dn" name="bind_dn" value="{{$cfg.BindDN}}" placeholder="e.g. cn=Search,dc=mydomain,dc=com">
									<p class="help text red">You can use '%s' as placeholder for username, e.g. DOM\%s</p>
								</div>
								<input class="fake" type="password">
								<div class="field">
									<label for="bind_password">Bind Password</label>
									<input id="bind_password" name="bind_password" type="password" value="{{$cfg.BindPassword}}">
									<p class="help text red">Warning: This password is stored in plain text. Do not use a high privileged account.</p>
								</div>
								<div class="required field">
									<label for="user_base">User Search Base</label>
									<input id="user_base" name="user_base" value="{{$cfg.UserBase}}" placeholder="e.g. ou=Users,dc=mydomain,dc=com" required>
								</div>
							{{end}}
							{{if .Source.IsDLDAP}}
								<div class="required field">
									<label for="user_dn">User DN</label>
									<input id="user_dn" name="user_dn" value="{{$cfg.UserDN}}" placeholder="e.g. uid=%s,ou=Users,dc=mydomain,dc=com" required>
								</div>
							{{end}}
							<div class="required field">
								<label for="filter">User Filter</label>
								<input id="filter" name="filter" value="{{$cfg.Filter}}" placeholder="e.g. (&(objectClass=posixAccount)(uid=%s))" required>
							</div>
							<div class="field">
								<label for="admin_filter">Admin Filter</label>
								<input id="admin_filter" name="admin_filter" value="{{$cfg.AdminFilter}}">
							</div>
							<div class="field">
								<label for="attribute_username">Username Attribute</label>
								<input id="attribute_username" name="attribute_username" value="{{$cfg.AttributeUsername}}" placeholder="Leave empty to use sign-in form field value for user name.">
							</div>
							<div class="field">
								<label for="attribute_name">First Name Attribute</label>
								<input id="attribute_name" name="attribute_name" value="{{$cfg.AttributeName}}">
							</div>
							<div class="field">
								<label for="attribute_surname">Surname Attribute</label>
								<input id="attribute_surname" name="attribute_surname" value="{{$cfg.AttributeSurname}}">
							</div>
							<div class="required field">
								<label for="attribute_mail">Email Attribute</label>
								<input id="attribute_mail" name="attribute_mail" value="{{$cfg.AttributeMail}}" placeholder="e.g. mail" required>
							</div>

							<div class="inline field">
								<div class="ui checkbox">
									<label><strong>Verify group membership</strong></label>
									<input class="enable-system" type="checkbox" name="group_enabled" data-target="#group_box" {{if $cfg.GroupEnabled}}checked{{end}}>
								</div>
							</div>
							<div class="ui segment field {{if not $cfg.GroupEnabled}}disabled{{end}}" id="group_box">
								<div class="field">
									<label for="group_dn">Group Search Base DN</label>
									<input id="group_dn" name="group_dn" value="{{$cfg.GroupDN}}" placeholder="e.g. ou=group,dc=mydomain,dc=com">
								</div>
								<div class="field">
									<label for="group_filter">Group Filter</label>
									<input id="group_filter" name="group_filter" value="{{$cfg.GroupFilter}}" placeholder="e.g. (|(cn=gitote_users)(cn=admins))">
								</div>
								<div class="field">
									<label for="group_member_uid">Group Attribute Containing List of Users</label>
									<input id="group_member_uid" name="group_member_uid" value="{{$cfg.GroupMemberUID}}" placeholder="e.g. memberUid">
								</div>
								<div class="field">
									<label for="user_uid">User Attribute Listed in Group</label>
									<input id="user_uid" name="user_uid" value="{{$cfg.UserUID}}" placeholder="e.g. uid">
								</div>
							</div>
							{{if .Source.IsLDAP}}
								<div class="inline field">
									<div class="ui checkbox">
										<label><strong>Fetch attributes in Bind DN context</strong></label>
										<input name="attributes_in_bind" type="checkbox" {{if $cfg.AttributesInBind}}checked{{end}}>
									</div>
								</div>
							{{end}}
						{{end}}

						<!-- SMTP -->
						{{if .Source.IsSMTP}}
							{{ $cfg:=.Source.SMTP }}
							<div class="inline required field">
								<label>SMTP Authentication Type</label>
								<div class="ui selection type dropdown">
									<input type="hidden" id="smtp_auth" name="smtp_auth" value="{{$cfg.Auth}}" required>
									<div class="text">{{$cfg.Auth}}</div>
									<i class="dropdown icon"></i>
									<div class="menu">
										{{range .SMTPAuths}}
											<div class="item" data-value="{{.}}">{{.}}</div>
										{{end}}
									</div>
								</div>
							</div>
							<div class="required field">
								<label for="smtp_host">SMTP Host</label>
								<input id="smtp_host" name="smtp_host" value="{{$cfg.Host}}" required>
							</div>
							<div class="required field">
								<label for="smtp_port">SMTP Port</label>
								<input id="smtp_port" name="smtp_port" value="{{$cfg.Port}}" required>
							</div>
							<div class="field">
								<label for="allowed_domains">Allowed Domains</label>
								<input id="allowed_domains" name="allowed_domains" value="{{$cfg.AllowedDomains}}">
								<p class="help">Leave it empty to not restrict any domains. Multiple domains should be separated by comma ','.</p>
							</div>
						{{end}}

						<!-- PAM -->
						{{if .Source.IsPAM}}
							{{ $cfg:=.Source.PAM }}
							<div class="required field">
								<label for="pam_service_name">PAM Service Name</label>
								<input id="pam_service_name" name="pam_service_name" value="{{$cfg.ServiceName}}" required>
							</div>
						{{end}}

						<div class="inline field {{if not .Source.IsSMTP}}hide{{end}}">
							<div class="ui checkbox">
								<label><strong>Enable TLS Encryption</strong></label>
								<input name="tls" type="checkbox" {{if .Source.UseTLS}}checked{{end}}>
							</div>
						</div>
						<div class="has-tls inline field {{if not .HasTLS}}hide{{end}}">
							<div class="ui checkbox">
								<label><strong>Skip TLS Verify</strong></label>
								<input name="skip_verify" type="checkbox" {{if .Source.SkipVerify}}checked{{end}}>
							</div>
						</div>
						<div class="inline field">
							<div class="ui checkbox">
								<label><strong>This authentication is activated</strong></label>
								<input name="is_active" type="checkbox" {{if .Source.IsActived}}checked{{end}}>
							</div>
						</div>
						<div class="inline field">
							<div class="ui checkbox">
								<label><strong>This authentication is default login source</strong></label>
								<input name="is_default" type="checkbox" {{if .Source.IsDefault}}checked{{end}}>
							</div>
						</div>

						<div class="field">
							<button class="ui green button">Update Authentication Setting</button>
							{{if not .Source.LocalFile}}
								<div class="ui red button delete-button" data-url="{{$.Link}}/delete" data-id="{{.Source.ID}}">Delete This Authentication</div>
							{{end}}
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui small basic delete modal">
	<div class="ui icon header">
		<i class="trash icon"></i>
		Authentication Deletion
	</div>
	<div class="content">
		<p>This authentication is going to be deleted, do you want to continue?</p>
	</div>
	{{template "base/delete_modal_actions" .}}
</div>
{{template "base/footer" .}}
