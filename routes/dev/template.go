package dev

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"gitote/gitote/pkg/setting"
)

//TemplatePreview pre render the template to preview it
func TemplatePreview(c *context.Context) {
	c.Data["User"] = models.User{Name: "Yoginth"}
	c.Data["AppVer"] = setting.AppVer
	c.Data["APIVer"] = setting.APIVer
	c.Data["AppURL"] = setting.AppURL
	c.Data["Code"] = "123456789"
	c.Data["ActiveCodeLives"] = setting.Service.ActiveCodeLives / 60
	c.Data["ResetPwdCodeLives"] = setting.Service.ResetPwdCodeLives / 60
	c.Data["CurDbValue"] = ""

	c.HTML(200, (c.Params("*")))
}
