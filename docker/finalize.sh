#!/bin/sh
# Finalize the build

set -x
set -e

# Create git user for Gitote
addgroup -S git
adduser -G git -H -D -g 'Gitote Git User' git -h /data/git -s /bin/bash && usermod -p '*' git && passwd -u git
echo "export GITOTE_CUSTOM=${GITOTE_CUSTOM}" >> /etc/profile

# Final cleaning
rm -rf /app/gitote/build
rm /app/gitote/docker/build.sh
rm /app/gitote/docker/build-go.sh
rm /app/gitote/docker/finalize.sh
rm /app/gitote/docker/nsswitch.conf
