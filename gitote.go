// +build go1.7

/*

   _____ _ _        _
  / ____(_) |      | |
 | |  __ _| |_ ___ | |_ ___
 | | |_ | | __/ _ \| __/ _ \
 | |__| | | || (_) | ||  __/
  \_____|_|\__\___/ \__\___|

*/

package main

import (
	"gitote/gitote/cmd"
	"gitote/gitote/pkg/setting"
	"os"

	raven "github.com/getsentry/raven-go"
	"github.com/urfave/cli"
)

// AppVer represents the version of Gitote
const AppVer = "1.0.1-prod-rc.5"

// APIVer represents the API version of Gitote
const APIVer = "v1"

func init() {
	raven.SetDSN("https://c711d9b100e74dfeb5fdee84a4d6c273:ff5ccef0870a48aea106d89b67a69710@sentry.io/1268720")
	setting.AppVer = AppVer
	setting.APIVer = APIVer
}

func main() {
	app := cli.NewApp()
	app.Name = "Gitote"
	app.Usage = "Software version control made simple"
	app.Version = AppVer
	app.Commands = []cli.Command{
		cmd.Web,
		cmd.Serv,
		cmd.Hook,
		cmd.Cert,
		cmd.Admin,
		cmd.Import,
	}
	app.Run(os.Args)
}
