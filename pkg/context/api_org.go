package context

import (
	"gitote/gitote/models"
)

type APIOrganization struct {
	Organization *models.User
	Team         *models.Team
}
