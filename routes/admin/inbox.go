package admin

import (
	"gitote/gitote/pkg/context"
)

const (
	// INBOX page template
	INBOX = "admin/inbox"
)

// Inbox shows admin inbox page
func Inbox(c *context.Context) {
	c.Data["Title"] = "Inbox"
	c.Data["PageIsAdminInbox"] = true
	c.HTML(200, INBOX)
}
