Execute following command in ROOT directory when anything is changed:

```sh
# Download Bindata
$ wget https://gitlab.com/gitote/bindata/raw/master/go-bindata

# Build Bindata
$ make bindata
```