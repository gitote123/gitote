package form

import (
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
)

type AdminCrateUser struct {
	LoginType  string `binding:"Required"`
	LoginName  string
	UserName   string `binding:"Required;AlphaDashDot;MaxSize(35)"`
	Email      string `binding:"Required;Email;MaxSize(254)"`
	Password   string `binding:"MaxSize(255)"`
	SendNotify bool
}

func (f *AdminCrateUser) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

type AdminEditUser struct {
	LoginType        string `binding:"Required"`
	LoginName        string
	FullName         string `binding:"MaxSize(100)"`
	Company          string
	Description      string
	ThemeColor       string
	Email            string `binding:"Required;Email;MaxSize(254)"`
	Password         string `binding:"MaxSize(255)"`
	Website          string `binding:"MaxSize(50)"`
	Location         string `binding:"MaxSize(50)"`
	Status           string `binding:"MaxSize(50)"`
	StaffNotes       string `binding:"MaxSize(255)"`
	Twitter          string `binding:"MaxSize(50)"`
	Linkedin         string `binding:"MaxSize(50)"`
	Github           string `binding:"MaxSize(50)"`
	Devto            string `binding:"MaxSize(50)"`
	Stackoverflow    string `binding:"MaxSize(50)"`
	Reddit           string `binding:"MaxSize(50)"`
	Telegram         string `binding:"MaxSize(50)"`
	Codepen          string `binding:"MaxSize(50)"`
	Gitlab           string `binding:"MaxSize(50)"`
	Recognized       string
	Certified        string
	MaxRepoCreation  int
	Active           bool
	Admin            bool
	AllowGitHook     bool
	AllowImportLocal bool
	Suspended        bool
	IsVerified       bool
	IsMaker          bool
	IsBugHunter      bool
	GitoteDeveloper  bool
	IsBeta           bool
	IsStaff          bool
	IsIntern         bool
}

func (f *AdminEditUser) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}
