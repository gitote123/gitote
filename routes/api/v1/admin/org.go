package admin

import (
	"gitote/gitote/pkg/context"
	"gitote/gitote/routes/api/v1/org"
	"gitote/gitote/routes/api/v1/user"

	api "gitlab.com/gitote/go-gitote-client"
)

func CreateOrg(c *context.APIContext, form api.CreateOrgOption) {
	org.CreateOrgForUser(c, form, user.GetUserByParams(c))
}
