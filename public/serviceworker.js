var STATIC_CACHE = 'gitote-sw-cache';
var urlsToCache = [
  // js
  '/js/gitote.js',
  'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/jquery.AreYouSure/1.9.0/jquery.are-you-sure.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js',
  'https://cdn.jsdelivr.net/npm/gitote@1.0.2/plugins/highlight/highlight.pack.js',
  'https://cdnjs.cloudflare.com/ajax/libs/emojify.js/1.1.0/js/emojify.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.9/clipboard.min.js',
  'https://cdn.jsdelivr.net/npm/notebookjs@0.3.0/notebook.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.6/marked.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/simplemde/1.10.1/simplemde.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.17.0/addon/mode/loadmode.js',
  'https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.17.0/mode/meta.js',
  'https://cdnjs.cloudflare.com/ajax/libs/autosize.js/4.0.2/autosize.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js',
  'https://cdn.jsdelivr.net/npm/gitote@1.0.2/plugins/datetimepicker/jquery.datetimepicker.js',
  'https://cdnjs.cloudflare.com/ajax/libs/jquery-minicolors/2.2.3/jquery.minicolors.min.js',

  
  // css
  '/css/gitote.css',
  '/css/bundle.css',
  'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css',
  'https://cdn.jsdelivr.net/npm/gitote@1.0.3/plugins/octicons/octicons.min.css',
  'https://cdn.jsdelivr.net/npm/gitote@1.0.2/plugins/highlight/github.css',
  'https://cdnjs.cloudflare.com/ajax/libs/simplemde/1.10.1/simplemde.min.css',
  'https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css',
  'https://cdn.jsdelivr.net/npm/gitote@1.0.2/plugins/datetimepicker/jquery.datetimepicker.css',
  'https://cdnjs.cloudflare.com/ajax/libs/jquery-minicolors/2.2.3/jquery.minicolors.min.css',
  
  // img
  '/fluidicon.png',
  '/og.png',
  'https://cdn.jsdelivr.net/npm/gitote@1.0.1/logo.png',
  'https://cdn.jsdelivr.net/npm/gitote@1.0.1/img/404.png',
  'https://cdn.jsdelivr.net/npm/gitote@1.0.1/img/500.png',
  
  // misc
  '/favicon.ico',
];

self.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open(STATIC_CACHE)
      .then(function (cache) {
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        if (response) {
          return response;
        }
        return fetch(event.request);
      }
      )
  );
});