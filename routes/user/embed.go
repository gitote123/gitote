package user

import (
	"gitote/gitote/pkg/context"
	"strings"
)

const (
	EMBED = "embed/user"
)

func Embed(c *context.Context) {
	ctxUser := GetUserByName(c, strings.TrimSuffix(c.Params(":username"), ""))

	c.Data["Title"] = ctxUser.DisplayName()
	c.Data["PageIsUserProfile"] = true
	c.Data["Owner"] = ctxUser

	c.HTML(200, EMBED)
}
