<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="referrer" content="no-referrer" />
<meta name="_csrf" content="{{.CSRFToken}}" />
<meta name="_suburl" content="{{AppSubURL}}" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="hostname" content="{{AppURL}}">

{{if .PageIsAdmin}}
{{else if .PageIsUserProfile}}
<link rel="canonical" href="{{.Owner.HTMLURL}}" />
{{else if .PageIsIssueConversation}}
<link rel="canonical" href="{{$.Link}}/{{.Index}}" />
{{else if .Repository}}
<link rel="canonical" href="{{.Repository.HTMLURL}}" />
{{else}}
<link rel="canonical" href="{{AppURL}}" />
{{end}}

{{if .PageIsAdmin}}
{{else if .PageIsUserProfile}}

<meta name="title" content="{{.Owner.Name}}{{if .Owner.FullName}} ({{.Owner.FullName}}){{end}}">
<meta name="description" content="{{.Owner.Name}} has {{.Owner.NumRepos}} repositories available. Follow their code on Gitote.">
<meta name="keywords" content="Gitote, {{.Owner.Name}}, {{.Owner.FullName}}">

<meta property="og:site_name" content="Gitote" />
<meta property="og:url" content="{{.Owner.HTMLURL}}" />
<meta property="og:type" content="profile" />
<meta property="og:title" content="{{.Owner.Name}}{{if .Owner.FullName}} ({{.Owner.FullName}}){{end}}">
<meta property="og:description" content="{{.Owner.Name}} has {{.Owner.NumRepos}} repositories available. Follow their code on Gitote.">
<meta property="og:image" content="{{.Owner.AvatarLink}}?s=250" />

<meta property="twitter:card" content="summary" />
<meta property="twitter:title" content="{{.Owner.Name}}{{if .Owner.FullName}} ({{.Owner.FullName}}){{end}}">
<meta property="twitter:description" content="{{.Owner.Name}} has {{.Owner.NumRepos}} repositories available. Follow their code on Gitote.">
<meta property="twitter:image" content="{{.Owner.AvatarLink}}?s=250" />

{{else if .PageIsIssueConversation}}

<meta name="title" content="{{.Title}} · Issue #{{.Issue.Index}} · {{.Repository.FullName}}">
<meta name="description" content="{{if .Issue.RenderedContent}}{{IssueDesc .Issue.RenderedContent|Str2html}}{{end}}">

<meta property="og:site_name" content="Gitote" />
<meta property="og:url" content="{{$.Link}}/{{.Index}}" />
<meta property="og:type" content="object" />
<meta property="og:title" content="{{.Title}} · Issue #{{.Issue.Index}} · {{.Repository.FullName}}">
<meta property="og:description" content="{{if .Issue.RenderedContent}}{{IssueDesc .Issue.RenderedContent|Str2html}}{{end}}">
<meta property="og:image" content="{{.Issue.Poster.RelAvatarLink}}" />

<meta property="twitter:card" content="summary" />
<meta property="twitter:title" content="{{.Title}} · Issue #{{.Issue.Index}} · {{.Repository.FullName}}">
<meta property="twitter:description" content="{{if .Issue.RenderedContent}}{{IssueDesc .Issue.RenderedContent|Str2html}}{{end}}">
<meta property="twitter:image" content="{{.Issue.Poster.RelAvatarLink}}" />

{{else if .Repository}}

<meta name="title" content="{{.Repository.FullName}}">
<meta name="description" content="{{.Repository.Description}}">
<meta name="keywords" content="Gitote, {{.Owner.Name}}, {{.Owner.FullName}}, {{.RepoName}}, {{.Owner.Name}}/{{.RepoName}}">

<meta property="og:site_name" content="Gitote" />
<meta property="og:url" content="{{.Repository.HTMLURL}}" />
<meta property="og:type" content="object" />
<meta property="og:title" content="{{.Repository.FullName}}">
<meta property="og:description" content="{{.Repository.Description}}">
<meta property="og:image" content="{{.Repository.Owner.AvatarLink}}" />

<meta property="twitter:card" content="summary" />
<meta property="twitter:title" content="{{.Repository.FullName}}">
<meta property="twitter:description" content="{{.Repository.Description}}">
<meta property="twitter:image" content="{{.Repository.Owner.AvatarLink}}" />

{{else}}

<meta name="title" content="{{if .IsLogged}}{{.Title}}{{else}}{{if .Title}}{{.Title}} - {{end}}Gitote | {{.i18n.Tr "app_desc"}}{{end}}">
<meta name="description" content="{{.i18n.Tr "description"}}">
<meta name="keywords" content="Gitote, developer, git, vcs">

<meta property="og:url" content="{{AppURL}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{if .IsLogged}}{{.Title}}{{else}}{{if .Title}}{{.Title}} - {{end}}Gitote | {{.i18n.Tr "app_desc"}}{{end}}">
<meta property="og:description" content="{{.i18n.Tr "description"}}">
<meta property="og:image" content="{{AppURL}}og.png" />
<meta property="og:site_name" content="Gitote">

<meta property="twitter:card" content="summary" />
<meta property="twitter:title" content="{{if .IsLogged}}{{.Title}}{{else}}{{if .Title}}{{.Title}} - {{end}}Gitote | {{.i18n.Tr "app_desc"}}{{end}}">
<meta property="twitter:description" content="{{.i18n.Tr "description"}}">
<meta property="twitter:image" content="{{AppURL}}og.png" />

{{end}}