package user

import (
	"encoding/xml"
	"net/http"
)

type Urlset struct {
	Loc        string `xml:"url>loc"`
	Changefreq string `xml:"url>changefreq"`
	Priority   string `xml:"url>priority"`
}

func Sitemap(w http.ResponseWriter, r *http.Request) {
	profile := Urlset{"https://gitote.in/", "Daily", "1.0"}

	x, err := xml.MarshalIndent(profile, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/xml")
	w.Write(x)
}
