{{if .Owner.IsIntern}}
    <div class="page" size="A4" layout="portrait">
	<div class="margin">
		<div class="container">

			<div class="box a">
				<div class="cert-to">
					<h3>Certified To</h3>
				</div>
				<div class="cert-name">
					<h1>{{.Owner.FullName}}</h1>
				</div>
				<div class="cert-p">
					<p class="w230">Certificate of Completion and Is Globally Recognized As:</p>
				</div>
				<div class="cert-type">
					<h2>{{.Owner.Recognized}}</h2>
				</div>
				<div class="cert-url">
					<p><strong>Certification Authenticity Validation:</strong>
                    <span>https://gitote.in/certificate/{{.Owner.Name}}</span></p>
				</div>
				<div class="cert-legal">
					<p class="legal-copy"><b>© 2018 - Present, Gitote all rights reserved.</b> This documentation and its intellectual property belongs to Gitote. Any unauthorized use, reproduction, preparation of other works based on this document, dissemination or representation
						of software presented in this document without the express written permission of Gitote is strictly prohibited. Gitote and Gitote branding are owned by Gitote. Other brands, services and business names belong to their
						respective companies.</p>
				</div>

			</div>

			<div class="box b">
				<div class="align-center">
					<div class="cert-logo">
						<p><img style="height:100px; margin-bottom:20px" src="https://cdn.jsdelivr.net/npm/gitote@1.1.9/logo.png"><br>
                        <span class="cert-slogan">Software version control made simple!</span></p>
					</div>
					<div class="cert-signature signature-1"><img src="https://cdn.jsdelivr.net/npm/gitote@1.2.0/img/sign.jpg">
						<p><b>Yoginth</b><br> <span class="position">Founder and CEO</span></p>
					</div>
				</div>
				<div class="certificate-data">
					<div class="certificate-dates">
						<div>
							<p><span class="label">Certification Date:</span><br><span class="date">{{.Owner.Certified}}</span></p>
						</div>
					</div>
					<div class="certificate-qr">
						<div class="cert-qr"><img src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=http://gitote.local/interns/certificate/{{.Owner.Name}}&choe=UTF-8" alt="QR Code"></div>
						<div>
							<p style="text-transform:uppercase"><b>OTE{{.Owner.Name}}{{.Owner.ID}}</b></p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

    <style>
    @import url('https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i');

    body {
        margin: 40px;
        /*background: rgb(204, 204, 204);*/
        background:#443b63;
        font-family: 'Noto Sans';
        font-size:10pt;
    }

    h1,h2,h3,h4,h5,h6 {
        margin:0;
        display: inline-block;
        font-weight:normal;
    }

    div,p,span,h1,h2,h3,h4,h5,h6 { color: #625b7c; }

    h1, h2 { font-size:24pt; letter-spacing:-.38px; }

    .page {
        background: white;
        display: block;
        margin: 0px auto;
    }

    .margin {
        width:10in;
        height:6.0in;
        border:2px solid red;
        position: relative;
        top:.4in;
        left:.45in;
        border: 2px solid #625b7c;
        border-radius: 0.09in;
        padding:3px;
    }
    .container {
        display: grid;
        grid-gap: 0;
        grid-template-columns: 7.2in 2.8in;
        background-color: #fff;
        color: #444;
    }

    .a {
        grid-column: 1 / 2;
        grid-row: 1;
    }
    .b {
        grid-column: 2;
        grid-row: 1;
        /*background:cyan;*/
    }

    .certificate-data {
        display: grid;
        grid-gap: 0;
        grid-template-columns: 50% 50%;
        position:absolute;
        width:2.8in;
        margin-top:3.25em;
    }

    .cert-to, .cert-name, .cert-p,
    .cert-type, .cert-url,
    .cert-legal {
        position:relative;
        padding-left:26pt;
        padding-right:26pt;
    }

    .cert-to 				{ height:44pt; margin-bottom:20pt; }
    .cert-to h3 		{ position:absolute; bottom:0; }
    .cert-name 			{ height:82pt; }
    .cert-name h1		{ font-weight:bold; }
    .cert-p .w230 	{ min-height:40pt; }
    .cert-type			{ min-height:50pt; margin-bottom:30pt; }
    .cert-type h2 	{ font-weight:normal; }
    .cert-url				{ font-size:7pt;margin-top:17.25em; }

    .cert-logo			{ width:130pt; height:130pt; 
                                        padding-top:20pt; padding-bottom:50pt; }
    .cert-logo p 		{ line-height:1; }
    .cert-slogan		{ font-size:7pt; font-weight:bold; line-height:1.2;}
    .cert-logo .cert-slogan { display:inline-block; width:80%; }

    .cert-signature { font-size:7pt; padding-bottom:15pt; }
    .cert-signature p { border-top: 2px solid #ddd; width:60%; padding-top:1em; /*top:-10pt; position:relative;*/ }
    .cert-signature img { max-width:160pt; height:5em; margin-bottom:4px; }
    .cert-signature .position { color: #7a7a7a; }

    .certificate-dates .label { font-size:8pt; color:#7a7a7a;}
    .certificate-dates .date { font-weight:bold; }
    .certificate-dates { margin-top: 7em; }

    .cert-qr img { width: 80pt; height:80pt; }
    /*
    Utilities
    */

    .w230 				{ width: 230pt; }
    .legal-copy 	{ font-size:6pt; }
    .align-center * { text-align:center; margin: 0 auto; }



    div.page[size="A4"] {
    width: 8.2in;
    height: 11in;
    }

    div.page[size="A4"][layout="portrait"] {
    width: 11in;
    height: 6.9in;
    }

    @page {
        size: A4 landscape;
    }
    </style>
{{else}}
    <div class="pages bg">
        {{template "base/head" .}}
        <div class="pages body">
            <div class="ui center">
                <img class="ui centered medium image" src="https://cdn.jsdelivr.net/npm/gitote@1.2.1/img/svg/certificate.svg">
                <h1>You don't have any certificate yet!</h1>
            </div>
        </div>
        {{template "base/footer" .}}
    </div>
{{end}}
