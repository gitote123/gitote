# Deployment

#### Git

```sh
git pull origin master # Production
git pull origin develop # Development
```

#### Update Gitote

```sh
cd $GOPATH/src/gitote/gitote
make bindata
go build
```

#### Restart Services

```sh
sudo systemctl restart gitote.service
sudo systemctl restart nginx.service
```

#### Check Services Status

```sh
sudo systemctl status gitote.service
sudo systemctl status nginx.service
```

###### Happy Coding ❤️
