// +build miniwinsvc

package setting

import (
	_ "gitlab.com/gitote/minwinsvc"
)

func init() {
	SupportMiniWinService = true
}
