#!/bin/sh
set -x
set -e

# Set temp environment vars
export GOPATH=/tmp/go
export PATH=/usr/local/go/bin:${PATH}:${GOPATH}/bin

# Install build deps
apk --no-cache --no-progress add --virtual build-deps build-base

# Build Gitote
mkdir -p ${GOPATH}/src/gitote/gitote/
ln -s /app/gitote/build ${GOPATH}/src/gitote/gitote
cd ${GOPATH}/src/gitote/gitote

# Needed since git 2.9.3 or 2.9.4
git config --global http.https://gopkg.in.followRedirects true
make build

# Cleanup GOPATH
rm -r $GOPATH

# Remove build deps
apk --no-progress del build-deps

# Move to final place
mv /app/gitote/build/gitote /app/gitote/

# Cleanup go
rm -rf /tmp/go
rm -rf /usr/local/go
