<!DOCTYPE html>
<html lang="{{.Lang}}">
<head data-suburl="{{AppSubURL}}">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<link rel="dns-prefetch" href="https://cdn.jsdelivr.net">
	<link rel="dns-prefetch" href="https://cdnjs.cloudflare.com">
	{{template "base/meta" .}}
	<link rel="icon" href="{{AppURL}}favicon.ico" type="image/x-icon"/>
	<link rel="fluid-icon" href="{{AppURL}}fluidicon.png" title="Gitote">
	<link rel="apple-touch-icon" href="{{AppURL}}black.png"/>
	<link rel="search" type="application/opensearchdescription+xml" href="/search.xml" title="Gitote">

	<script>
	if ('serviceWorker' in navigator) {
		window.addEventListener('load', function() {
			navigator.serviceWorker.register('/serviceworker.js').then(function(registration) {
				console.log('ServiceWorker registration successful with scope: ', registration.scope);
				console.log('Gitote 💖\'s web️')
			}, function(err) {
				console.log('ServiceWorker registration failed: ', err);
			});
		});
	}
 	</script>
 	
 	{{template "base/verification" .}}

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.AreYouSure/1.9.0/jquery.are-you-sure.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/gitote@1.0.3/plugins/octicons/octicons.min.css">

	{{if .IsIPythonNotebook}}

	<script src="https://cdn.jsdelivr.net/npm/notebookjs@0.3.0/notebook.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.6/marked.min.js"></script>

	{{end}}

	{{if .RequireSimpleMDE}}

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simplemde/1.10.1/simplemde.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/simplemde/1.10.1/simplemde.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.17.0/addon/mode/loadmode.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.17.0/mode/meta.js"></script>
	<script>
		CodeMirror.modeURL =  "https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.17.0/mode/%N/%N.js";
	</script>

	{{end}}

	{{if .PageIsUserSettings}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.min.js"></script>
	{{end}}

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/gitote@1.2.4/css/bundle.min.css">
	<link rel="stylesheet" href="{{AppSubURL}}/css/gitote.min.css?v={{MD5 AppVer}}">
	<noscript>
		<style>
			.dropdown:hover > .menu { display: block; }
			.ui.secondary.menu .dropdown.item > .menu { margin-top: 0; }
			{{if not .IsLogged}}
			.full.height {
				margin-bottom: -94px !important;
			}
			{{end}}
		 </style>
	</noscript>
	{{if not .IsLogged}}
	<style>
		.full.height { margin-bottom: -94px }
	</style>
	{{end}}

	<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/gitote@1.3.2/js/gitote.min.js?v={{MD5 AppVer}}"></script>

	{{if not .IsLogged}}
		{{if .PageIsHome}}
			<title>{{if .Title}}{{.Title}} - {{end}}Gitote | {{.i18n.Tr "app_desc"}}</title>
		{{else}}
			<title>{{if .Title}}{{.Title}} - {{end}}Gitote</title>
		{{end}}
	{{else}}
		<title>{{if .Title}}{{.Title}} - {{end}}Gitote</title>
	{{end}}

	<meta name="theme-color" content="#1e2327">

</head>
<body>
	<div class="full height">
		<noscript>This website works better with JavaScript</noscript>
		{{if not .PageIsInstall}}
		{{if not .PageIsSuspended}}
			<div class="following bar light" style="{{if not .IsLogged}}padding-top:5px;padding-bottom:5px{{end}}">
				{{if or .IsAdmin .IsStaff .IsIntern}}
					{{template "misc/admin_bar" .}}
				{{end}}
				<div class="navbar container">
					<div class="ui grid">
						<div class="column">
							<div class="ui top secondary menu">
								<a class="item brand" href="{{AppSubURL}}/">
									<img class="ui mini image" src="https://cdn.jsdelivr.net/npm/gitote@1.0.5/logo.svg">
								</a>
								{{if or .IsBeta .IsAdmin .IsStaff}}
									<span class="item navbar beta">β</span>
								{{end}}
								
								{{if .IsLogged}}
									<a class="navbar bold main-menu item{{if .PageIsDashboard}} nav-active{{end}}" href="{{AppSubURL}}/"><i class="menu-icon octicon">🏠</i>{{.i18n.Tr "dashboard"}}</a>
									<a class="navbar bold main-menu item{{if .PageIsIssues}} nav-active{{end}}" href="{{AppSubURL}}/issues"><i class="menu-icon octicon">📑</i>{{.i18n.Tr "issues"}}</a>
									<a class="navbar bold main-menu item{{if .PageIsPulls}} nav-active{{end}}" href="{{AppSubURL}}/pulls"><i class="menu-icon octicon">🚧</i>{{.i18n.Tr "pull_requests"}}</a>
								{{else}}
									<a class="navbar bold main-menu item{{if .PageIsHome}} nav-active{{end}}" href="{{AppSubURL}}/"><i class="menu-icon octicon">🏠</i>{{.i18n.Tr "home"}}</a>
									<a class="navbar bold main-menu item{{if .PageIsFeatures}} nav-active{{end}}" href="{{AppSubURL}}/features"><i class="menu-icon octicon">🔮</i>{{.i18n.Tr "features"}}</a>
								{{end}}

								<a class="navbar bold main-menu item{{if or .PageIsExplore .PageIsTrending}} nav-active{{end}}" href="{{AppSubURL}}/trending"><i class="menu-icon octicon">🌐</i>{{.i18n.Tr "explore"}}</a>

								<form class="search-mobile item" action="{{AppSubURL}}/explore/repos" method="get">
									<div class="ui icon input">
										<input class="searchbox" type="text" name="q" placeholder="{{.i18n.Tr "explore.search"}}..." autocomplete="off" required>
										<i class="search icon"></i>
									</div>
								</form>

								{{if .IsLogged}}
									<div class="right menu">
										<div class="ui dropdown head link jump item head-link">
											<span class="text">
												<i class="menu-icon octicon">➕</i> <i class="navbar caret octicon octicon-triangle-down"></i>
											</span>
											<div class="menu">
												<a class="item" href="{{AppSubURL}}/repo/create">
													<i class="menu-icon octicon">📂</i>{{.i18n.Tr "new_repo"}}
												</a>
												<a class="item" href="{{AppSubURL}}/repo/migrate">
													<i class="menu-icon octicon">🎯</i>{{.i18n.Tr "new_migrate"}}
												</a>
												{{if .LoggedUser.CanCreateOrganization}}
												<a class="item" href="{{AppSubURL}}/org/create">
													<i class="menu-icon octicon">🏢</i>{{.i18n.Tr "new_org"}}
												</a>
												{{end}}
												{{if .PageIsRepoHome}}
												<div class="divider"></div>
												<a class="item" href="{{.RepoLink}}/issues/new">
													<i class="menu-icon octicon">📑</i>{{.i18n.Tr "repo.issues.new"}}
												</a>
												{{end}}
											</div>
										</div>

										<div class="ui dropdown head link jump item head-link">
											<span class="text avatar">
												<img class="ui small rounded image" src="{{.LoggedUser.RelAvatarLink}}">
												<span class="sr-only">{{.i18n.Tr "user_profile_and_more"}}</span>
												<i class="navbar caret octicon octicon-triangle-down" tabindex="-1"></i>
											</span>
											<div class="menu" tabindex="-1">
												<div class="ui header">
													<a class="navbar signedinas" href="{{AppSubURL}}/{{.LoggedUser.Name}}">
														{{.i18n.Tr "signed_in_as"}} <strong>{{.LoggedUser.Name}}</strong>
													</a>
												</div>

												<div class="divider"></div>
												<a class="{{if .PageIsUserProfile}}active{{end}} item" href="{{AppSubURL}}/{{.LoggedUser.Name}}">
													<i class="menu-icon octicon">👦</i>{{.i18n.Tr "your_profile"}}
												</a>
												<a class="{{if .PageIsUserSettings}}active{{end}} item" href="{{AppSubURL}}/user/settings">
													<i class="menu-icon octicon">🛠️</i>{{.i18n.Tr "your_settings"}}
												</a>
												{{if .IsAdmin}}
													<div class="divider"></div>

													<a class="{{if .PageIsAdmin}}active{{end}} item" href="{{AppSubURL}}/admin">
														<i class="menu-icon octicon">🚀</i>{{.i18n.Tr "admin_panel"}}
													</a>
												{{end}}

												<div class="divider"></div>
												<a class="{{if .PageIsHelp}}active{{end}} item" href="{{AppSubURL}}/help">
													<i class="menu-icon octicon">🙋</i>{{.i18n.Tr "help"}}
												</a>
												<a class="{{if .PageIsContribute}}active{{end}} item" href="{{AppSubURL}}/contribute">
													<i class="menu-icon octicon">❤️</i>{{.i18n.Tr "contribute"}}
												</a>
												{{if or .IsAdmin .IsStaff}}
													<div class="divider"></div>
													<a class="item" href="https://gitote.in/gitote/gitote">
														<i class="menu-icon octicon">👨‍💻</i>{{.i18n.Tr "sourcecode"}}
													</a>
													<a target="_blank" class="item" href="https://discordapp.com/channels/516272231643611147">
														<i class="menu-icon octicon">💬</i>{{.i18n.Tr "discord"}}
													</a>
												{{end}}
												{{if or .IsBeta .IsAdmin .IsStaff}}
													<div class="divider"></div>
													<div class="item">
														<i class="menu-icon octicon">🦊</i>{{.i18n.Tr "beta"}}
														<div class="menu">
															<a class="item" href="{{AppSubURL}}/security">
																<i class="menu-icon octicon">🐞</i>{{.i18n.Tr "reportbug"}}
															</a>
															<a class="item" href="{{AppSubURL}}/request">
																<i class="menu-icon octicon">⛲</i>{{.i18n.Tr "featurerequest"}}
															</a>
														</div>
													</div>
													<div class="divider"></div>
												{{end}}
												<a class="item" href="{{AppSubURL}}/user/logout">
													<i class="menu-icon octicon">👋</i>{{.i18n.Tr "sign_out"}}
												</a>
												<div class="divider"></div>
												<a style="margin-top:-8px" class="item" target="_blank" href="https://madewithlove.org.in">
													<span>
														Made with  <span style="color: #e74c3c">&hearts;</span> in India
													</span>
												</a>
											</div>
										</div>
									</div>

								{{else}}

									<div class="right menu hide-mobile">
										<a class="navbar bold home-main btn signin main-menu item{{if .PageIsSignIn}} active{{end}}" href="{{AppSubURL}}/login?redirect_to={{.Link}}">
											<i class="menu-icon octicon octicon">🔑</i> {{.i18n.Tr "sign_in"}}
										</a>
										{{if .ShowRegistrationButton}}
											<a class="navbar bold home-main btn register main-menu item{{if .PageIsSignUp}} active{{end}}" href="{{AppSubURL}}/join">
												<i class="menu-icon octicon">➕ </i> {{.i18n.Tr "register"}}
											</a>
										{{end}}
									</div>

								{{end}}
							</div>
						</div>
					</div>
				</div>
			</div>
		{{end}}
		{{end}}
{{/*
	</div>
</body>
</html>
*/}}
