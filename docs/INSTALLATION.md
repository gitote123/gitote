# Installation

This section provides a high-level requirement & quick start guide.

### Hardware Requirements

- A **Raspberry Pi** or $5 **Digital Ocean Droplet** is more than enough to get you started. Some even use 64MB RAM Docker [CaaS](https://blog.docker.com/2016/02/containers-as-a-service-caas/).
- 2 CPU cores and 512MB RAM would be the baseline for teamwork.
- Increase CPU cores when your team size gets significantly larger, memory footprint remains low.

### Prerequisites

##### [Go Lang](https://golang.org)

```sh
curl -O https://storage.googleapis.com/golang/go1.11.linux-amd64.tar.gz
sha256sum go1.11.linux-amd64.tar.gz
tar -xvf go1.11.linux-amd64.tar.gz
sudo chown -R root:root ./go
sudo mv go /usr/local
export GOPATH=$HOME/go
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
```

##### [Go Lang](https://golang.org) using [Snap](https://snapcraft.io/)

```sh
sudo apt install gcc
sudo snap install go --classic
```

##### [PostgreSQL](https://www.postgresql.org/) (9.4 or higher)

```sh
sudo apt install postgresql postgresql-contrib
sudo update-rc.d postgresql enable
sudo service postgresql start
sudo su - postgres
createdb gitote
psql
ALTER USER postgres PASSWORD 'myPassword'
ALTER ROLE
\password
\q
```

### Installation

1.  Make sure all the prerequisites are installed.
2.  Fork **Gitote** repository, ie. https://gitote.in/repo/fork/1

#### Create `Git` User

```sh
sudo adduser --disabled-login --gecos 'Gitote' git
```

#### Download and build Gitote

```sh
sudo su - git
mkdir -p $GOPATH/src/gitote
cd $GOPATH/src/gitote
git clone https://gitote.in/gitote/gitote.git
or
git clone root@gitote.in:gitote/gitote.git
cd gitote
wget https://gitlab.com/gitote/bindata/raw/master/go-bindata
make bindata
go build
./gitote web -p 8080
```

#### Compiling the LESS

We're mostly a go app, with a bit of **LESS** sprinkled in. **For most cases, simply running `make` will do.** If you're working with LESS though, you'll need to run the following:

```sh
sudo apt install node-less
```

* Run **`make less`** to compile all LESS files.

### Run Gitote in Production

##### Setting up Gitote as a Service

```sh
cp script/systemd/gitote.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl restart gitote.service
sudo systemctl status gitote.service

# Cron Setup
crontab -e
@reboot sudo systemctl restart gitote.service
```

##### Installing NGINX

```sh
sudo apt install nginx
cp script/conf/gitote /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/gitote /etc/nginx/sites-enabled/gitote
sudo systemctl restart nginx
```

##### Setting up HTTPS

```sh
sudo apt-get install python-certbot-nginx
sudo certbot --nginx
sudo certbot renew --dry-run # Renewal
```

###### Happy Coding ❤️
